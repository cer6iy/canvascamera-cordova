/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

app.initialize();


document.addEventListener("deviceready", function () {
    canvasMain = document.getElementById("camera");
    CanvasCamera.initialize(canvasMain);
    // define options
    var opt = {
        quality: 75,
        destinationType: CanvasCamera.DestinationType.DATA_URL,
        encodingType: CanvasCamera.EncodingType.JPEG,
        saveToPhotoAlbum: true,
        correctOrientation: true,
        width: 640,
        height: 480
    };
    CanvasCamera.start(opt);
});

function onChangeDevicePosition() {

    var newDevicePosition = CanvasCamera.CameraPosition.BACK;
    if (document.getElementById("deviceposition_back").checked) {
        newDevicePosition = CanvasCamera.CameraPosition.BACK;
    }
    else if (document.getElementById("deviceposition_front").checked) {
        newDevicePosition = CanvasCamera.CameraPosition.FRONT;
    }
    //
    CanvasCamera.setCameraPosition(newDevicePosition);
}

function onChangeFlashMode() {

    var newFlashMode = CanvasCamera.FlashMode.OFF;
    if (document.getElementById("flashmode_off").checked) {
        newFlashMode = CanvasCamera.FlashMode.OFF;
    }
    else if (document.getElementById("flashmode_on").checked) {
        newFlashMode = CanvasCamera.FlashMode.ON;
    }
    else if (document.getElementById("flashmode_auto").checked) {
        newFlashMode = CanvasCamera.FlashMode.AUTO;
    }

    CanvasCamera.setFlashMode(newFlashMode);
}

function onTakePicture() {
    CanvasCamera.takePicture(onTakeSuccess);
}

function onTakeSuccess(data) {
    console.log(data)
}